package com.ksga.ams.repository;

import com.ksga.ams.model.category.Category;
import com.ksga.ams.repository.provider.CategoryProvider;
import java.util.List;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository {
  @SelectProvider(type = CategoryProvider.class,method = "findAll")
  public List<Category> findAll();
  @SelectProvider(type = CategoryProvider.class,method = "findOne")
  public Category findById(int id);
  @InsertProvider (type = CategoryProvider.class,method = "insert")
  public boolean save(@Param("category") Category category);
  @UpdateProvider(type = CategoryProvider.class,method = "updateCategory")
  public boolean update(int id, Category category);
  @DeleteProvider(type = CategoryProvider.class,method = "deleteCategory")
  public boolean delete(int id);

}
