package com.ksga.ams.repository;


import com.ksga.ams.model.article.Article;
import com.ksga.ams.model.article.ArticleFilter;
import com.ksga.ams.repository.provider.ArticleProvider;
import com.ksga.ams.utilities.Paging;
import java.util.List;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository {
  @SelectProvider(type = ArticleProvider.class,method = "findAll")
  @Results(id = "articleMapping",
  value = {
      @Result(column = "image_url",property = "imageURL"),
      @Result(column = "category_id",property = "category.id"),
      @Result(column = "cuid",property = "category.cuid"),
      @Result(column = "name",property = "category.name")
  })
  public List<Article> findAll(@Param("paging")Paging paging);

  @SelectProvider(type = ArticleProvider.class,method = "findOne")
  @ResultMap("articleMapping")
  public Article findOne(@Param("id") String id);

//  @InsertProvider(type = ArticleProvider.class,method = "insert")
//  public boolean insert(@Param("article") Article article);

  @UpdateProvider(type =ArticleProvider.class,method = "update")
  public boolean update(String id, Article article);

  @DeleteProvider(type = ArticleProvider.class,method = "delete")
  public boolean delete(@Param("auid") String auid);

  @InsertProvider(type = ArticleProvider.class,method = "insert")
  @ResultMap("articleMapping")
//  @Options(useGeneratedKeys = true,keyColumn = "id",keyProperty = "id")
  public boolean insert(@Param("article") Article article);


  @SelectProvider (type = ArticleProvider.class,method = "findById")
  @ResultMap("articleMapping")
  public boolean findByUid(@Param("auid") int id);
  @Select("SELECT * FROM articles inner join categories c on articles.category_id = c.id WHERE articles.id = (SELECT MAX(articles.id) FROM articles)")
  @ResultMap("articleMapping")
  public Article findLastArticle();

  @SelectProvider(type = ArticleProvider.class,method = "findAllFilteredArticle")
  @ResultMap("articleMapping")
  public List<Article> findAllFilteredArticles(@Param("filter") ArticleFilter filter);
@SelectProvider(type = ArticleProvider.class,method = "countAll")
public int countAllRecord();
}
