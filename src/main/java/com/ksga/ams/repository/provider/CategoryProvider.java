package com.ksga.ams.repository.provider;

import com.ksga.ams.model.category.Category;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {
  public String findAll(){
    return new SQL(){{
      SELECT("*");
      FROM("categories");
    }}.toString();
  }

  public String findOne(int id){
    return new SQL(){{
      SELECT("*");
      FROM("categories");
      WHERE("id =#{id}");
    }}.toString();
  }

  public String insert(Category category){
    return new  SQL(){{
      INSERT_INTO("categories");
      VALUES("name","#{category.name}");
    }}.toString();
  }

  public String updateCategory(int id,Category category){
    return new SQL(){{
      UPDATE("categories");
      SET("name=#{categoy.name}");
      WHERE("id=#{id}");
    }}.toString();
  }


  public String deleteCategory(int id){
    return new SQL(){{
      DELETE_FROM("categories");
      WHERE("id=#{id}");
    }}.toString();
  }
}
