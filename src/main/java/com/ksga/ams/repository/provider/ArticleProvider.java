package com.ksga.ams.repository.provider;

import com.ksga.ams.model.article.Article;
import com.ksga.ams.model.article.ArticleFilter;
import com.ksga.ams.utilities.Paging;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;


public class ArticleProvider {


  public String findAll(@Param("paging") Paging paging){

    return new SQL(){{
      SELECT("*");
      FROM("articles");
      INNER_JOIN("categories on articles.category_id = categories.id");
      ORDER_BY("articles.id desc  limit #{paging.limit} offset #{paging.offset}");
    }}.toString();
  }
  public String findOne(@Param("id") String id){
    return new SQL(){{
      SELECT("*");
      FROM("articles");
      INNER_JOIN("categories on articles.category_id = categories.id");
      WHERE("articles.auid = #{id}");
    }}.toString();
  }

  public String insert(Article article){
    return new SQL(){{
      INSERT_INTO("articles");
      VALUES("title,description,image_url,category_id","#{article.title},#{article.description},#{article.imageURL},#{article.category.id}");

    }}.toString();
  }
  public String update(String id, Article article){
    return new SQL(){{
      UPDATE("articles a");
      SET("title=#{article.title},description=#{article.description},image_url=#{article.imageURL},category_id=#{article.category.id}");
      WHERE("a.auid=#{id}");
    }}.toString();
  }

  public String delete(String id){
    return new SQL(){{
      DELETE_FROM("articles");
      WHERE("articles.auid=#{id}");
    }}.toString();
  }

  public String findById(String auid){
    return new SQL(){{
      SELECT("*");
      FROM("articles");
      INNER_JOIN("categories on articles.category_id = categories.id");
      WHERE("articles.aid=#{auid}");
    }}.toString();
  }

  public String findAllFilteredArticle(@Param("filter") ArticleFilter filter){
    return new SQL(){{
      SELECT("*");
      FROM("articles");
      INNER_JOIN("categories on articles.category_id = categories.id");
      if(filter.getTitle()!=null){
        WHERE("articles.title ILIKE '%' || #{filter.title} || '%' ");
      }
      if(filter.getCategoryId()!=null){
        WHERE("articles.category_id = #{filter.categoryId}");
      }
      ORDER_BY("articles.id DESC ");
    }}.toString();
  }
  public String countAll(){
    return new SQL(){{
      SELECT("count(id)");
      FROM("articles");
    }}.toString();
  }


}
