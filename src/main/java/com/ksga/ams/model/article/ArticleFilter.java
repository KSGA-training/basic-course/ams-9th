package com.ksga.ams.model.article;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleFilter {
 private String title;
 public Integer categoryId;

}
