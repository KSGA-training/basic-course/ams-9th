package com.ksga.ams.model.article;

import com.ksga.ams.model.category.Category;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Data
@Getter
@Setter
@ToString
public class Article {
    private int id;
    private String auid;
    private String title;
    private String description;
    private String imageURL;
    private Category category;
}
