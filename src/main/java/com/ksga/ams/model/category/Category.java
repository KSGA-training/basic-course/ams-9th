package com.ksga.ams.model.category;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Category {
private int id;
private String cuid;
private String name;

  public Category(int id) {
    this.id = id;
  }
@JsonIgnore
  public int getId() {
    return id;
  }
}
