package com.ksga.ams.utilities;

import lombok.Data;
import org.springframework.http.HttpStatus;
@Data
public class ApiError {
  String message;
  HttpStatus httpStatus;

}
