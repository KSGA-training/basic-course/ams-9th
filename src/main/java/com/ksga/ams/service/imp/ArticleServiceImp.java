package com.ksga.ams.service.imp;

import com.ksga.ams.dto.article.ArticleDto;
import com.ksga.ams.dto.article.ArticleRequest;
import com.ksga.ams.mapping.ArticleMapper;
import com.ksga.ams.model.article.Article;
import com.ksga.ams.model.article.ArticleFilter;
import com.ksga.ams.repository.ArticleRepository;
import com.ksga.ams.service.ArticleService;
import com.ksga.ams.utilities.Paging;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ArticleServiceImp implements ArticleService {
private ArticleRepository repository;
private ArticleMapper articleMapper;
ModelMapper mapper = new ModelMapper();
@Autowired
  public ArticleServiceImp(ArticleRepository repository,
      ArticleMapper articleMapper) {
    this.repository = repository;
    this.articleMapper = articleMapper;
  }

  @Override
  public List<ArticleDto> findAll(Paging paging) {
  paging.setTotalCount(repository.countAllRecord());
  List<ArticleDto> dtoList = new ArrayList<>();
  for(Article a: repository.findAll(paging)){
    ArticleDto dto = mapper.map(a,ArticleDto.class);
    dtoList.add(dto);
  }
    return dtoList;
  }

  @Override
  public ArticleDto findOne(String id) {
  ArticleDto dto = mapper.map(repository.findOne(id),ArticleDto.class);
    return dto;
  }

  @Override
  public Article update(String id, Article article) {
    if(repository.update(id, article)){
      return repository.findOne(id);
    }
    else
    return null;
  }

  @Override
  public boolean delete(String id) {
    return repository.delete(id);
  }

  @Override
  public ArticleDto insert(ArticleRequest request) {
  Article a = articleMapper.mapRequestToModel(request);
    if(repository.insert(a)){
      ArticleDto dto = articleMapper.mapToDto(repository.findLastArticle());
      return dto;
    }
    else
    return null;
  }

  @Override
  public List<Article> findAllFilteredArticles(ArticleFilter filter) {
    return repository.findAllFilteredArticles(filter);
  }
}
