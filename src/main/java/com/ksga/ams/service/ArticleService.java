package com.ksga.ams.service;


import com.ksga.ams.dto.article.ArticleDto;
import com.ksga.ams.dto.article.ArticleRequest;
import com.ksga.ams.model.article.Article;
import com.ksga.ams.model.article.ArticleFilter;
import com.ksga.ams.utilities.Paging;
import java.util.List;



public interface ArticleService {

  public List<ArticleDto> findAll(Paging paging);

  public ArticleDto findOne(String id);

  public Article update(String id, Article article);

  public boolean delete(String id);

  public ArticleDto insert( ArticleRequest article);
  public List<Article> findAllFilteredArticles(ArticleFilter filter);

}
