package com.ksga.ams.dto.article;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleRequest {
  private String title;
  private String description;
  private String imageURL;
  private Integer categoryId;

}
