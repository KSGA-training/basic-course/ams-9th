package com.ksga.ams.dto.article;

import com.ksga.ams.model.category.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleDto {
  private String auid;
  private String title;
  private String description;
  private String imageURL;
  private Category category;
}
