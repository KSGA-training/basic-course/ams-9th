package com.ksga.ams.controller.rest;

import com.ksga.ams.model.category.Category;
import com.ksga.ams.repository.CategoryRepository;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/categories/")
public class CategoryRestController {
  private final CategoryRepository categoryRepository;

  public CategoryRestController(
      CategoryRepository categoryRepository) {
    this.categoryRepository = categoryRepository;
  }

  @GetMapping
  public List<Category> getAll(){
    return categoryRepository.findAll();
  }

  @PostMapping
  public Category insertNew(@RequestBody Category category){
    categoryRepository.save(category);
    return category;
  }

}
