package com.ksga.ams.controller.rest;

import com.ksga.ams.dto.article.ArticleDto;
import com.ksga.ams.dto.article.ArticleRequest;
import com.ksga.ams.model.article.Article;
import com.ksga.ams.model.article.ArticleFilter;
import com.ksga.ams.service.ArticleService;
import com.ksga.ams.utilities.ApiError;
import com.ksga.ams.utilities.Paging;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/articles")
public class ArticleRestController {

  private final ArticleService articleService;

@Autowired
  public ArticleRestController(
      ArticleService articleService) {

    this.articleService = articleService;
  }

  @GetMapping
  public ResponseEntity<Map<String,Object>> findAll(Paging paging){
  Map<String, Object> response = new HashMap<>();
  response.put("data",articleService.findAll(paging));
  response.put("pagination",paging);
  response.put("message","successfully fetched");
  response.put("status", HttpStatus.OK);
  return ResponseEntity.ok(response);
  }


  @PostMapping
  public ResponseEntity<Map<String, Object>> insertArticle(@RequestBody ArticleRequest article){
    Map<String, Object> response = new HashMap<>();

    response.put("message","successfully create");
    response.put("data",articleService.insert(article));
    response.put("status", HttpStatus.CREATED);
    return ResponseEntity.ok(response);


  }
  @PutMapping("/{id}")
  public Article update(@PathVariable String id, @RequestBody Article article){
   return articleService.update(id,article);

  }

  @DeleteMapping("/{id}")
  public String delete(@PathVariable String id)
  {
    articleService.delete(id);
    return "delete success";
  }

  @GetMapping("/{id}")
  public ArticleDto findById(@PathVariable String id){
  return articleService.findOne(id);
  }

  @GetMapping("/search")
  public List<Article> findAllFilteredArticles( ArticleFilter filter){
  return articleService.findAllFilteredArticles(filter);
  }





}
