package com.ksga.ams.controller;

import com.ksga.ams.service.ArticleService;
import com.ksga.ams.utilities.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class ArticleController {

private ArticleService articleService;
@Autowired
  public ArticleController(ArticleService articleService) {
    this.articleService = articleService;
  }

  @GetMapping("/")
  public String displayAll(ModelMap modelMap, Paging paging){
    System.out.println(paging);
  modelMap.addAttribute("articleList",articleService.findAll(paging));
  modelMap.addAttribute("paging",paging);
    System.out.println(articleService.findAll(paging));
  return "index";

  }
  @GetMapping("/article/view/{uid}")
  public String viewDetail(@PathVariable String uid,ModelMap modelMap){
  modelMap.addAttribute("article",articleService.findOne(uid));
  return "view";
  }

  @GetMapping("/ajax")
  public String showAjax(){
  return "ajax";
  }
}
