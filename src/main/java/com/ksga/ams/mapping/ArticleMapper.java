package com.ksga.ams.mapping;

import com.ksga.ams.dto.article.ArticleDto;
import com.ksga.ams.dto.article.ArticleRequest;
import com.ksga.ams.model.article.Article;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface ArticleMapper {

//  @Mapping(target = "category",expression = "java(article.getCategory())")
  ArticleDto mapToDto(Article article);


//  @Mapping(source = "categoryId",target = "category.id")
  Article mapRequestToModel(ArticleRequest request);

}
