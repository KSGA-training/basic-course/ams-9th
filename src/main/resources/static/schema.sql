create table categories(id int primary key auto_increment, name varchar(150));
create  table articles(id int primary key auto_increment,
                       title varchar(150),description varchar(200),image_url varchar(200),
                       category_id int references categories(id) on delete cascade on update cascade );

insert into categories(name) values ( 'Sport' );
insert into categories(name) values ( 'Entertainment' );
insert into categories(name) values ( 'Education' );
insert into categories(name) values ( 'Public Health' );



INSERT into articles (title,description,category_id,image_url) values ('ទស្សនា ការ ប៉េណាល់ ទី ដល់ ទៅ ២១ គ្រាប់ ទើប រក ឃើញ ជើង ឯក Europa League','Villarreal បាន ក្លាយ ជា ជើង ឯង Europa League រដូវ កាល ២០២០-២១ បន្ទាប់ ពី បាន យក ឈ្នះ Manchester United ដោយ បាល់ ប៉េណាល់ទី កាត់ សេចក្ដី ១១-១០។',1,'Default.jpg');
INSERT into articles (title,description,category_id,image_url) values ('ទស្សនា ការ ប៉េណាល់ ទី ដល់ ទៅ ២១ គ្រាប់ ទើប រក ឃើញ ជើង ឯក Europa League','Villarreal បាន ក្លាយ ជា ជើង ឯង Europa League រដូវ កាល ២០២០-២១ បន្ទាប់ ពី បាន យក ឈ្នះ Manchester United ដោយ បាល់ ប៉េណាល់ទី កាត់ សេចក្ដី ១១-១០។',1,'Default.jpg');
INSERT into articles (title,description,category_id,image_url) values ('ទស្សនា ការ ប៉េណាល់ ទី ដល់ ទៅ ២១ គ្រាប់ ទើប រក ឃើញ ជើង ឯក Europa League','Villarreal បាន ក្លាយ ជា ជើង ឯង Europa League រដូវ កាល ២០២០-២១ បន្ទាប់ ពី បាន យក ឈ្នះ Manchester United ដោយ បាល់ ប៉េណាល់ទី កាត់ សេចក្ដី ១១-១០។',1,'Default.jpg');
INSERT into articles (title,description,category_id,image_url) values ('ទស្សនា ការ ប៉េណាល់ ទី ដល់ ទៅ ២១ គ្រាប់ ទើប រក ឃើញ ជើង ឯក Europa League','Villarreal បាន ក្លាយ ជា ជើង ឯង Europa League រដូវ កាល ២០២០-២១ បន្ទាប់ ពី បាន យក ឈ្នះ Manchester United ដោយ បាល់ ប៉េណាល់ទី កាត់ សេចក្ដី ១១-១០។',1,'Default.jpg');
